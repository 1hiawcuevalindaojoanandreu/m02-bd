# Caso Representantes de ventas (Training)
---
## Descripción de las tablas de la base de datos de prácticas

La base de datos relacional con la que vamos a trabajar en las prácticas
consta de cinco relaciones o tablas denominadas CLIENTE, OFICINA,
PEDIDO, PRODUCTO y REPVENTA.

Los atributos o campos de cada una de estas tablas se especifican a
continuación:

CLIENTE(<ins>CLIECOD</ins>, NOMBRE, REPCOD, LIMCRED)

OFICINA(<ins>OFINUM</ins>, CIUDAD, REGION, DIRECTOR, OBJETIVO, VENTAS)

PEDIDO(<ins>PEDNUM</ins>, FECHA, CLIECOD, REPCOD, FABCOD, PRODCOD, CANT, IMPORTE)

PRODUCTO(<ins>FABCOD, PRODCOD</ins>, DESCRIP, PRECIO, EXIST)

REPVENTA(<ins>REPCOD</ins>,NOMBRE, EDAD, OFINUM, PUESTO, FCONTRATO, JEFE,
CUOTA ,VENTAS)

La información contenida en estas tablas corresponde a una empresa de
venta de recambios. La empresa tiene varias oficinas situadas en
distintas ciudades. Los empleados son representantes que se dedican a
vender los productos de distintos fabricantes a sus clientes. A
continuación se detalla la información contenida en cada una de las
tablas de la base de datos, con lo que se comprenderá mejor el
funcionamiento de la empresa y cuáles son los datos de su interés.

__CLIENTE__ es la tabla con información sobre los clientes. Cada cliente
tiene asignado un código único CLIECOD. Del cliente interesa saber el
NOMBRE, el representante (REPCOD) que contactó con él por primera vez y
el límite de crédito que se le puede conceder (LIMCRED),

__OFICINA__ es la tabla que tiene los datos sobre cada una de las oficinas
que la empresa posee. Cada oficina tiene asignado un número único
OFINUM. De ella interesa saber su nombre, que es el de la CIUDAD en que
está situada, la REGION a la que vende (Este, Oeste), el representante
director de la oficina y el total acumulado del importe de las
VENTAS realizadas por los representantes asignados a la misma. Además,
cada oficina tiene marcado un OBJETIVO de venta, que corresponde al
total del importe de las ventas que se pretende alcanzar por los
representantes de la oficina.

__PEDIDO__ es la tabla donde se guarda la información referente a los
pedidos realizados a la empresa. A cada pedido se le asigna un número
que es único (PEDNUM). Los datos de un pedido son los siguientes: FECHA
en que se tomó el pedido, el cliente que lo realizó (CLIECOD), el
representante que hizo la venta (REPCOD), el producto solicitado
(FABCOD, PRODCOD es clave primaria), cantidad de unidades pedidas (CANT)
e IMPORTE total del pedido. En cada pedido se solicita un sólo tipo de
producto.

__PRODUCTO__ es la tabla que contiene los datos acerca de los productos que
la empresa vende, Estos productos le son suministrados por distintos
fabricantes, cada uno de los cuales tiene un código distinto (FABCOD).
Cada fabricante utiliza unos códigos para denominar sus productos
(PRODCOD). Ya que cabe la posibilidad de que dos fabricantes distintos
utilicen los mismos códigos de producto, la empresa de venta de
recambios utiliza ambos códigos, el del fabricante y el del producto
(FABCOD,PRODCOD), para identificar de modo único los artículos que
vende. De cada uno de ellos se guarda una descripción (DESCRIP), el
PRECIO por unidad y las existencias del mismo que hay en el almacén
(EXIST).

__REPVENTA__ es la tabla donde se guardan los
datos de los representantes de la empresa. Cada uno tiene asignado un
código que es único (REPCOD). De él se quiere saber el NOMBRE, la EDAD,
la oficina a la que está asignado (OFINUM), el PUESTO que ocupa, la
fecha de contrato (FCONTRATO), su jefe, la CUOTA de
ventas que debe alcanzar y el total del importe de las VENTAS que ha
realizado.

![casPractic](casPractic.png)

La cardinalidad de todas estas relaciones es de uno a muchos (1:n):

1\. Cada representante pertenece a una sola oficina; en cada oficina
trabajan varios representantes.

2\. Cada representante tiene un solo jefe; un representante
puede dirigir (ser jefe) a varios representantes.

3\. Cada oficina es dirigida por un solo director; un representante puede
dirigir varias oficinas.

4\. Cada pedido es solicitado por un solo cliente; un cliente puede
solicitar varios pedidos.

5\. Cada pedido es tomado por un solo representante; un representante
puede tomar varios pedidos.

6\. Cada pedido solicita un solo producto; un producto puede ser
solicitado en varios pedidos.

7\. Cada cliente es contactado por primera vez por un solo representante;
un representante puede haber contactado por primera vez con varios
clientes.

# Ejercicios (empezad por el apartado `G-DML`)
---
## A. Consultas Simples

1\. Obtener los datos de los productos cuyas existencias estén entre 25 y
40 unidades.

2\. Obtener los códigos de los representantes que han tomado algún pedido
(evitando su repetición).

3\. Obtener los datos de los pedidos realizados por el cliente cuyo
código es el 2111.

4\. Obtener los datos de los pedidos realizados por el cliente cuyo
código es el 2111 y que han sido tomados por el representante cuyo
código es el 103.

5\. Obtener los datos de los pedidos realizados por el cliente cuyo
código es el 2111, que han sido tomados por el representante cuyo código
es el 103 y que solicitan artículos del fabricante cuyo código es ACI.

6\. Obtener una lista de todos los pedidos ordenados por cliente y, para
cada cliente, ordenados por la fecha del pedido (ascendentemente)

7\. Obtener los datos de los representantes que pertenecen a la oficina
de código 12 y 13 (cada representante solo pertenece a una oficina).

8\. Obtener los datos de productos de los que no hay existencias o bien
éstas son desconocidas.

9\. Mostrar los representantes que fueron contratados en el 2003 (sumem
5000 a la data de contracte)

10\. Mostrar el nombre y días que lleva contratados los representants

## B. Consultas Multitabla

1.  Muestra de los representantes su nombre, la ciudad de su oficina así como su región.

2.  Obtener una lista de todos los pedidos, mostrando el número de
    pedido, su importe, el nombre del cliente que lo realizó y el límite
    de crédito de dicho cliente.

3.  Obtener una lista de representantes ordenada alfabéticamente,
    en la que se muestre el nombre del representante, codigo de la
    oficina donde trabaja, ciudad y la región a la que vende.

4.  Obtener una lista de las oficinas (ciudades, no códigos) que tienen
    un objetivo superior a 360000 euros. Para cada oficina mostrar la
    ciudad, su objetivo, el nombre de su director y puesto del mismo.

5.  Obtener una lista de todos los pedidos mostrando su número, el
    importe y la descripción de los productos solicitados.

6.  Obtener una lista de los pedidos con importes superiores
	a 4000. Mostrar el nombre del cliente que solicitó el pedido,
	numero del pedido, importe del mismo, la descripción del producto
	solicitado y el nombre del representante que lo tomó. Ordenad la
	lista por cliente alfabéticamente y luego por importe de mayor a menor.

7.  Obtener una lista de los pedidos con importes superiores
	a 2000 euros, mostrando el número de pedido, importe, nombre del
	cliente que lo solicitó y el nombre del representante que contactó
	con el cliente por primera vez.

8.  Obtener una lista de los pedidos con importes superiores a 150
    euros, mostrando el código del pedido, el importe, el nombre del
    cliente que lo solicitó, el nombre del representante que contactó
    con él por primera vez y la ciudad de la oficina donde el
    representante trabaja.

9.  Lista los pedidos tomados durante el mes de octubre del año 2003 ,
    mostrando solamente el número del pedido, su importe, el nombre del
    cliente que lo realizó, la fecha y la descripción del producto
    solicitado

10. Obtener una lista de todos los pedidos tomados por representantes de
    oficinas de la región Este, mostrando solamente el número del
    pedido, la descripción del producto y el nombre del representante
    que lo tomó

11. Obtener los pedidos tomados en los mismos días en que un nuevo
    representante fue contratado. Mostrar número de pedido, importe,
    fecha pedido.

12. Obtener una lista con parejas de representantes y oficinas en donde
    la cuota del representante es mayor o igual que el objetivo de la
    oficina, sea o no la oficina en la que trabaja. Mostrar Nombre del
    representante, cuota del mismo, Ciudad de la oficina, objetivo de la
    misma.

13. Muestra el nombre, las ventas y la ciudad de la oficina de cada
    representante de la empresa.

14. Obtener una lista de la descripción de los productos para los que
    existe algún pedido en el que se solicita una cantidad mayor a las
    existencias de dicho producto.

15. Lista los nombres de los representantes que tienen una cuota
    superior a la de su director.

16. Obtener una lista de los representantes que trabajan en una oficina
    distinta de la oficina en la que trabaja su director, mostrando
    también el nombre del director y el código de la oficina donde
    trabaja cada uno de ellos.

17. El mismo ejercicio anterior, pero en lugar de ofinum, la ciudad.

18. Mostrar el nombre y el puesto de los que son jefe.

## C. Funciones de grupo

1\. Mostrar la suma de las cuotas y la suma de las ventas totales de
todos los representantes.

2\. ¿Cuál es el importe total de los pedidos tomados por Bill Adams?

3\. Calcula el precio medio de los productos del fabricante ACI.

4\. ¿Cuál es el importe medio de los pedido solicitados por el cliente
2103

5\. Mostrar la cuota máxima y la cuota mínima de las cuotas de los
representantes.

6\. ¿Cuál es la fecha del pedido más antiguo que se tiene registrado?

7\. ¿Cuál es el mejor rendimiento de ventas de todos los representantes?
(considerarlo como el porcentaje de ventas sobre la cuota).

8\. ¿Cuántos clientes tiene la empresa?

9\. ¿Cuántos representantes han obtenido un importe de ventas superior a
su propia cuota?

10\. ¿Cuántos pedidos se han tomado de más de 150 euros?

11\. Halla el número total de pedidos, el importe medio, el importe total
de los mismos.

12\. ¿Cuántos puestos de trabajo diferentes hay en la empresa?

13\. ¿Cuántas oficinas de ventas tienen representantes que superan sus
propias cuotas?

14\. ¿Cuál es el importe medio de los pedidos tomados por cada
representante?

15\. ¿Cuál es el rango de las cuotas de los representantes asignados a
cada oficina (mínimo y máximo)?

16\. ¿Cuántos representantes hay asignados a cada oficina? Mostrar Ciudad
y número de representantes.

17\. ¿Cuántos clientes ha contactado por primera vez cada representante?
Mostrar el código de representante, nombre y número de clientes.

18\. Calcula el total del importe de los pedidos solicitados por cada
cliente a cada representante.

19\. Lista el importe total de los pedidos tomados por cada
representante.

20\. Para cada oficina con dos o más representantes, calcular el total de
las cuotas y el total de las ventas de todos sus representantes.

21\. Muestra el número de pedidos que superan el 75% de las existencias.

## D. Subconsultas

0. Mostrar el nombre y el puesto de los que son jefe (ya está hecho
con self join, ahora con subconsultas)

1\. Obtener una lista de los representantes cuyas cuotas son iguales ó
superiores al objetivo de la oficina de Atlanta.

2\. Obtener una lista de todos los clientes (nombre) que fueron
contactados por primera vez por Bill Adams.

3\. Obtener una lista de todos los productos del fabricante ACI cuyas
existencias superan a las existencias del producto 41004 del mismo
fabricante.

4\. Obtener una lista de los representantes que trabajan en las oficinas
que han logrado superar su objetivo de ventas.

5\. Obtener una lista de los representantes que no trabajan en las
oficinas dirigidas por Larry Fitch.

6\. Obtener una lista de todos los clientes que han solicitado pedidos
del fabricante ACI entre enero y junio de 2003.

7\. Obtener una lista de los productos de los que se ha tomado un pedido
de 150 euros ó mas.

8\. Obtener una lista de los clientes contactados por Sue Smith que no
han solicitado pedidos con importes superiores a 18 euros.

9\. Obtener una lista de las oficinas en donde haya algún representante
cuya cuota sea más del 55% del objetivo de la oficina. Para comprobar vuestro
ejercicio, haced una Consulta previa cuyo resultado valide el ejercicio.

10\. Obtener una lista de los representantes que han tomado algún pedido
cuyo importe sea más del 10% de de su cuota.

11\. Obtener una lista de las oficinas en las cuales el total de ventas
de sus representantes han alcanzado un importe de ventas que supera el
50% del objetivo de la oficina. Mostrar también el objetivo de cada
oficina (suponed que el campo ventas de oficina no existe).


## E. Intersección, unión y diferencia

1.  Obtener una lista de todos los productos cuyo precio exceda de 20
    euros y de los cuales hay algún pedido con un importe superior a 200
    euros.
2.  Obtener una lista de todos los productos cuyo precio más IVA exceda
    de 20 euros o bien haya algún pedido cuyo importe más IVA exceda los
    180 euros.
3.  Obtener los códigos de los representantes que son directores de
    oficina y que no han tomado ningún pedido.
4. Mostrar el representante que vende más y el que vende menos.

## F. Ejercicios Extra
0. Clientes que no han hecho ningún pedido.  
1\. Obtener una lista con los nombres de las oficinas en las que ningún
representante haya tomado pedidos de productos del fabricante BIC.  
2\. Obtener los nombre de los clientes que han solicitado pedidos a
representantes de oficinas que venden a la región Oeste o que fueron
contactados por primera vez por los directores de dichas oficinas.  
3\. Obtener los nombres de los clientes que sólo han hecho pedidos al
representante que contactó con ellos la primera vez.  
4\. Obtener los nombres de los clientes que han solicitado todos sus
pedidos a representantes que pertenecen a la misma oficina.  
5\. Obtener para cada oficina la cantidad de unidades vendidas por sus
representantes de productos del fabricante ACI ( de las oficinas se
muestra el nombre).  
6\. Mostrar una lista con los nombres de los representantes junto con los
nombres se sus directores. Si un representante no tiene director,
también ha de aparecer en la lista (evidentemente, a su lado no
aparecerá ningún nombre).  

## G. Ejercicios DML

1. El fabricante REI ha fabricado 100 altavoces de 65€, con código 3G123
2. Tom Snyder pasa a tener cuota, que es equivalente a un 25% de su salario.
3. A todas las oficinas del Oeste se les sube su objetivo un 15%.
4. Hoy se contrata a Andrew Bynum, de 30 años, su número de representante es 111,trabaja de Rep Ventas y tiene una cuota de 1800. Todavía no se sabe ni su jefe ni a que oficina irá.
5. La fecha del contrato de Paul Cruz se modifica y pasa a ser el dia 11/12/2013

6. Se despide a Sue Smith. Para ello, lo descomponemos en las siguientes tareas:  
  6.1. Crear un 'Sin Representante' para sustituirlo por Sue Smith.  
  6.2. Los clientes que estaban asignados a Sue Smith pasarlos a 'Sin Representante'.  
  6.3. Los pedidos que estaban realizados por Sue Smith pasarlos a 'Sin Representante'.  
  6.4. La/s oficina/s que tenia asignada a Sue Smith pasarla a 'Sin Representante'.  
  6.5. Se despide a Sue Smith.  


## H. Més exercicis

1. Muestra los pedidos que han sido tomados por el mismo representante que contactó por primera vez con el cliente.
2. TOP 5 de los que tienen mejor rendimiento.
3. Mostrar por cada oficina, su mejor vendedor.
4. TOP 5 de los que tienen peor rendimiento.
5. Mostrar para cada jefe, cuantos empleados directos tiene a su cargo.

## I. Funcions (__funcionsTraining.sql__)

Caldrà veure si cal crear seqüències per les claus primàries.

Per crear una seqüència per una clau primària d'una taula, la podeu inicialitzar a 1. Però com que en el nostre cas ja tenim dades a les taules, és imprescindible fer a continuació el següent (us poso exemple de la taula client):

```
select setval('cliecod_seq', (select max(cliecod) from cliente), true);
```
Aquesta sentència farà que la propera vegada que demanem el següent valor de la seqüència torni max+1, ja que tenim com a tercer paràmetre true [sequence functions](https://www.postgresql.org/docs/current/functions-sequence.html)

`Nota`: Les seqúències s'han de crear __obligatòriament__ fora de les funcions

**Funció**|`existeixClient`
---|---
Paràmetres|p_cliecod  
Tasca|comprova si existeix el client passat com argument  
Retorna|booleà

**Funció**|`altaClient`
---|---  
Paràmetres|p_nombre ,p_repcod, p_limcred
Tasca|Donarà d’alta un client
Retorna|Missatge _Client X s’ha donat d’alta correctament_
Nota| si no està creada, creem una seqüència per donar valors a la clau primària. Començarà en el següent valor que hi hagi a la base de dades.

**Funció**|`stockOk`
---|---
Paràmetres|p_cant , p_fabcod,p_prodcod
Tasca|Comprova que hi ha prou existències del producte demanat.
Retorna|booleà

**Funció**|`altaComanda`
---|---
Paràmetres| Segons els exercicis anteriors i segons necessitat, definiu vosaltres els paràmetres mínims que necessita la funció, tenint en compte que cal contemplar l'opció per defecte de no posar data, amb el què agafarà la data de sistema. Si no hi és, creeu una seqüència per la clau primària de pedido.  
Tasca| Per poder donar d'alta una comanda es tindrà que comprovar que existeix el client i que hi ha prou existències. En aquesta funció heu d'utilitzar les funcions  existeixClient i stockOK (recordeu de no posar `select function(...` ). Evidentment, s'haura de calcular el preu de l'import en funció del preu unitari i de la quantitat d'unitats.  
Retorna|missatge indicant el que ha passat  


**Funció**|`preuSenseIVA`
---|---
Paràmetres| p_precio (preu `amb` IVA)
Tasca| Donat un preu amb IVA, es calcularà el preu *sense* IVA (es considera un 21 % d'IVA) i serà retornat.

**Funció**|`preuAmbIVA`
---|---
Paràmetres| p_precio (preu `sense` IVA)
Tasca| Donat un preu sense IVA, es calcularà el preu *amb* IVA (es considera un 21 % d'IVA) i serà retornat.

# J. Triggers (__triggersTraining.sql__)

1. Implementeu el trigger `tActualitzarVendes` el qual es dispararà quan es fagi una nova comanda. Haurà d'actualitzar els camps calculats vendes de les taules repventa i oficina.

2. Implementeu el trigger `tControlProducte` per impedir fer qualsevol operació DML sobre la taula pedido fora de l'horari comercial (de dilluns a divendres de 9 a 14 i de 17-20, dissabtes de 10-14h). Proveu la funció to_char amb una data i el patró `d`.
